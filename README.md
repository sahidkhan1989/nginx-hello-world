# nginx-hello-world #



### What is this Repository For ###

To create and configure an Environment to host a simple Hello World static web page running on a Docker Container


### Dependencies ###


* Vagrant - https://www.vagrantup.com/
* Python 2.7 - https://www.python.org/download/releases/2.7/
* Ansible - https://www.ansible.com/
* VirtualBox - https://www.virtualbox.org/wiki/Downloads


### Deployment Instructions ###

Git Clone the Repo in an environment where HTTP and SSH is enabled

Navigate into the main folder 'nginx-hello-world'

`vagrant up`



### Tear Down Instructions ###

Navigate to the main folder 'nginx-hello-world'

`vagrant destroy`
